<?php
//show errors
ini_set('display_errors',1);

error_reporting(E_ALL);

//Get item ID

$pst_id_v = $_POST['pst_id'];

require_once('global/connection.php');

//delete item from database
$query=
"DELETE FROM petstore
WHERE pst_id = :pst_id_p";

try
{
	$statement = $db->prepare($query);
	$statement->bindParam(':pst_id_p', $pst_id_v);
	$row_count=$statement->execute();
	$statement->closeCursor();
	
	//view rows affected, comment when done testing 
	
	header('Location: index.php'); //sometimes, redirecting is needed
	}
	
catch (PDOException $e)
{

	$error = $e->getMessage();
	echo $error;
	}
	?>