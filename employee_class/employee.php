<?php

class Employee extends person
{
  private $ssn;
  private $gender;

//constructor
  public function __construct($fn = "Peter", $ln ="Paulsen", $ag = 33, $s = '123456789', $g = 'male')
  {
    $this->ssn = $s;
    $this->gender = $g;

    parent::__construct($fn, $ln, $ag);

    echo("Creating <strong>" .person::GetFname(). " " .person::GetLname(). " is " .person::GetAge(). " with ssn: " .$this->ssn. " and is " .$this->gender. "</strong> employee object from parameterized constructor (accepts five arguments): <br/>");
  }//end __construct

//destructor
    function __destruct()
    {
    parent::__destruct();
    echo("Destroying <strong>" .person::GetFname(). " " .person::GetLname(). " is " .person::GetAge(). " with ssn " .$this->ssn. " and is " .$this->gender. "</strong> employee object. <br/>");
  }//end destructor

//mutator methods
//set ssn
  public function SetSSN ($s = "111111111")
  {
    $this->ssn = $s;
  }//end SSN set

//set gender
  public function SetGender ($g = 'f')
  {
    $this->gender = $g;
  }//end gender set

//accessor methods 
//get ssn
  public function GetSSN()
  {
    return $this->ssn;
  }//end get SSN

//get gender
  public function GetGender()
  {
    return $this->gender;
  }//end get Gender
}
 ?>
