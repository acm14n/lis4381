
# LIS 4381

## Alexis Marrero

1. [Assignment 1](a1/README.md "My A1 READ.ME file")
    * Instal AMPPS
    * Instal JDK
    * Instal Android Studio and create My First App
    * Instal Bitbucket repo
    * Create Bitbucket Tutorials 
	*Provide git command descriptions
    
2. [Assignment 2](a2/README.md "My A2 READ.ME file")
    * Create Healthy Recipes Andriod app
    * Provide screenshots of completed app
    * Changed background color of application
    * Create two web pages

3. [Assignment 3](a3/README.md "A3 README.md file")

    * Create ERD based upon business rules
    * Provide screenshot of completed ERD
    * Provide DB resource links
    
    
4. [Project 1](p1/README.md "P1 README.md file")

    * Create business card application
    * Display details and contact information
    * Add border around picture and button

5. [Assignment 4](a4/README.md "A4 README.md file")

	* Create index.php file
	* Add jQuery validation expressions per entity attribute requirements
	* Carousel with responsive images using boostrap

6. [Assignment 5](a5/README.md "A5 README.md file")

    * Server-side validation
    * SQL forward engineering  
    * Create error.php

7. [Project 2](p2/README.md "P2 README.md file")
    
    * Server-side validation
    * Edit and delete functionalities  
    * connection to database