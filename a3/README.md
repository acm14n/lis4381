#LIS 4381

## Alexis Marrero

### Assignment 3 Requirements:


![a3 sql image](img/a3.png)

![a3 display 1 file](img/display1.png)

![a3 mwb file](img/display2.png)



### Assignment Links 

1. [a3 mwb file](docs/a3.mwb)

2. [a3 sql](docs/a3.sql)