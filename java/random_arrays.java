import java.util.Scanner;
import java.util.Random;

public class random_arrays
{
	public static void main(String[] args)
	{
	
	//display message
        System.out.println("Program prompts user to enter a desired number of pseudorandom-generated integers");
        System.out.println("Note: Use following loops structures: for, enhanced for, while. Posttests loop: do while");
        System.out.println(); //blank line
        
    Scanner sc = new Scanner(System.in);
    Random shuffle = new Random();
    int arraySize = 0;
   
    
    System.out.print("Enter desired numbers of generated integers: ");
	arraySize = sc.nextInt();
	
	int myArray[] = new int[arraySize];

	//for loop

        System.out.println("for loop:");

        for(int i=0; i <myArray.length; i++)
        {

                System.out.println(shuffle.nextInt());
        }

	System.out.println("\nEnhanced for loop:");
        for(int n: myArray)
        {
                System.out.println(shuffle.nextInt());
        }

    System.out.println("\nwhile loop:");
        
        int i=0;
        
        while (i<myArray.length)
                {
                System.out.println(shuffle.nextInt());
                i++;
                }
	//do while
        i=0;
        System.out.println("\ndo...while loop:");

        do
        {
                System.out.println(shuffle.nextInt());
                i++;
        } while (i < myArray.length);
    }
}


