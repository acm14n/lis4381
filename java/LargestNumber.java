import java.util.Scanner;

public class LargestNumber
{

	public static void main(String[] args)
	{
	System.out.println("Program evaluates largest of two integers.");
    System.out.println("Note: Program does *not* check for non-numeric characters.");
	System.out.println(); //blank line
	
	//declare scanner variables and create scanner object
	
	int num1,num2;
	Scanner sc = new Scanner(System.in);
	
	System.out.print("Enter first integer: ");
	num1 = sc.nextInt();
	
	System.out.print("Enter second integer: ");
	num2 = sc.nextInt();
	
	System.out.print(); //print blank line
	
	if ( num1 > num2)
	System.out.println(num1 + " is larger than " + num2);
	else if ( num2 > num1)
	System.out.println(num2 + " is larger than" + num1);
	
	}
}