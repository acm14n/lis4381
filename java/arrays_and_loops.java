import java.util.Scanner;

public class arrays_and_loops
{
	public static void main(String[] args)
	{
	//display message
	System.out.println("Program loops through array of strings");
	System.out.println("Note: Pretest loops: for, enhanced for, while. Posttests loop: do while");
	System.out.println(); //blank line
	
	//create for loop for an array
	
	String animal[] = {"dog","cat","bird","fish","insect"};
	
	System.out.println("for loop:");
	
	for(int i=0; i < animal.length; i++)
	{
		System.out.println(animal[i]);
	}
	
	//enhanced loop
	
	System.out.println("\nEnhanced for loop:");
	
	for(String test: animal)
		{
			System.out.println(test);
		}
		
	//while loop
	
	System.out.println("\nwhile loop:");
	int i=0;
	while (i<animal.length)
		{
		System.out.println(animal[i]);
		i++;
		}
		
		
		//do while loop 
		
		i=0;
	System.out.println("\ndo...while loop:");	
	
	do
	{
		System.out.println(animal[i]);
		i++;
	} while (i < animal.length);
		
		}
	
	}
