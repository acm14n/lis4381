class Person
{
//instance variables (no static keyword): each object has own set of instance variables 
private String fname;
private String lname;
private int age;

//public Person()
{
System.out.println("\nInside person default constructor."):
fname = "John";
lname = "Doe";
age = 21;
}
//parameterized constructor
public Person(String fname, String lname, int age)
{
System.out.println("\nInside person constructor with parameters.");
//must use keyword when parameter names are same as field names
this.fname = fname;
this.lname = lname;
this.age = age;
}

public String getFname()
{
 return fname;
 }
 
 public void setFname(String fn)
 {
 //set instance variable value to parameter value 
 fname = fn;
 }
 
 public String getLname()
 {
 return lname;
 }
 
 public void setLname(String ln)
 {
 //set instance varaible value to parameter value
 lname = ln;
 }
 
 public int getAge()
 {
 return age;
 }
 
 public void setAge(int a)
 {
 
//set instance variable value to parameter value 
age=a;

}

public void print()
{
System.out.println("\nFname: " + fname + ", Lname: " + lname + ", Age: " + age);
   }
   
}   