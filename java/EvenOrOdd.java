import java.util.Scanner;

public class EvenorOdd
{
	public static void main(String[] args)
	{
	//display message
	System.out.println("Program evaluates integers as even or odd.");
	System.out.println("Note: Program does *not* check for non-numeric characters.");
	System.out.println(); //blank line
	
	//initiallize var and create scanner object
	
	int x = 0;
	System.out.print("Enter integer: ");
	Scanner sc = new Scanner(System.in);
	x = sc.nextInt();
	
	if ( x % 2 == 0)
		{
		System.out.println(x + "is an even number.");
		}
	else
		{
		System.out.println(x + "is an odd number.");
		}
		