# LIS 4381

## Alexis Marrero

### Assignment 1 Requirements:

1. Ordered-list items
2. id_rsa & id_rsa.pub
3. /Users/alexismarrero/

> #### Git commands w/short descriptions:

1. git init creates an empty Git repository or reinitializes an existing one.
2. git status shows the working tree status
3. git add adds file contents to an index
4. git commit redords changes to the repository
5. git push updates remote refs along with associated objects
6. git pull fetch from and inegrate with another repository or local branch.
7. git var shows a git logical variable.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
